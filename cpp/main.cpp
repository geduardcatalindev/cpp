#include <iostream>
#include <string>

#include "DialogueTree.h"

void breakAutoConsoleClose();

int main(int argc, char *argv[])
{
	DialogueTree dialogueTree;

	dialogueTree.init();

	int rv = dialogueTree.performDialogue();

	if (rv == 1)
	{
		std::cout << "You accepted the quest!" << std::endl;
	}

	dialogueTree.destroyTree();

	breakAutoConsoleClose();

	return 0;
}

void breakAutoConsoleClose ()
{
	std::string exitCode;
	std::cout << std::endl << "Enter exit code: ";
	std::cin >> exitCode;
	while (exitCode != "exit")
	{
		std::cout << std::endl << "Enter exit code: ";
		std::cin >> exitCode;
	}
}
