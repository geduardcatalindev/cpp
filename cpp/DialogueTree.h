#pragma once

#include <iostream>
#include <string>
#include <vector>

class DialogueNode;

class DialogueOption
{
public:
	DialogueOption(std::string Text, int ReturnCode, DialogueNode *NextNode);

	std::string text;
	int returnCode;
	DialogueNode *nextNode;
};

class DialogueNode
{
public:
	DialogueNode(std::string Text);

	std::string text;
	std::vector<DialogueOption> dialogueOptions;
};

class DialogueTree
{
public:
	DialogueTree();

	void init();
	void destroyTree();

	int performDialogue();

private:
	std::vector <DialogueNode *> dialogueNodes;
};
